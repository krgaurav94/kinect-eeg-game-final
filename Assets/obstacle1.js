import System.IO;
public var log:GameObject;
public var lampleft:GameObject;
public var player:GameObject;
public var markRock : Transform;
public var marklampleft : Transform;
public var ledge : GameObject;
public var markLedge: Transform;
public var marklampright : Transform;
public var lampright : GameObject;
public var height: Vector3;
      
height=Vector3(0,6,0); 
var temp2 : GameObject;
public var dist:float;
var i:int;
var j:int;
var k:int;
var h : int;
var status_l:int;
var status_f:int;
var status_g : int;
var status_s :int;
var delay : float =10;
var flag : boolean = false;
var flagL : boolean = false;
var flagM : boolean =false;
var falgg: boolean = false;
var flagdie : boolean = false;
var flaggui : boolean =false;
var count : int;
var x : float;
var y : float;
var score:float;
var optscore: int;
var highScore: int;


var audio1:AudioClip;
var audio2:AudioClip;
var audio3:AudioClip;
var temp1:Vector3;


function Start ()
{
temp1=player.transform.position;


i=0;
j=0;
k=0;
h=0;
x=500;
y=200;
count =0;
status_l=0;
status_f=0;
status_g=0;
status_s=0;
score=-2;
optscore=0;
highScore=0;
 log=GameObject.Find("rock");
 //player=GameObject.Find("Robot Kyle");
 //lampleft=GameObject.Find("lampleft");
 ledge=GameObject.Find("ledge");
}


function valueGet():int {
var line : String;
var sr = new StreamReader("Test0File.txt");
line = sr.ReadLine();
sr.Close();
var i = parseInt(line);
return i;
}



function valueSet(HighScore : int)
{
var sw = new StreamWriter("Test0File.txt");
sw.WriteLine(HighScore);
sw.Close();
}


function Update ()
{
if(!flagdie)
score+=Vector3.Distance(temp1,player.transform.position);
temp1=player.transform.position;


optscore=score/5;

if(transform.position.y<-10){
	print("fell");
	flagdie=true;}
	
if(Input.GetKeyDown(KeyCode.R)){
var clone:GameObject;
clone=Instantiate(log,markRock.position,markRock.rotation);
clone.name="rock"+i;
flag=true;
i++;
count++;
}

if(Input.GetKeyDown(KeyCode.T)){
flagM=true;
clone=Instantiate(lampleft,marklampleft.position,marklampleft.rotation);
clone.name="lampleft"+j;
j++;
count++;
}

if(Input.GetKeyDown(KeyCode.G)){
clone=Instantiate(lampright,marklampright.position,marklampright.rotation);
clone.name="lampright"+h;
h++;
flagg=true;
count++;
}

if(Input.GetKeyDown(KeyCode.Y)){
	clone=Instantiate(ledge,markLedge.position,markLedge.rotation);
	//clone.transform.position=Vector3.Lerp(clone.transform.position,markLedge.position,delay*Time.deltatime);
	clone.name="ledge"+k;
	k++;
	flagL=true;
	count++;
}

if(flag && GameObject.Find("rock"+status_l).transform.position.y<-5){
Destroy(GameObject.Find("rock"+status_l));
print("rock"+status_l+"destroyed");
status_l++;
}

if(status_l==i)
flag=false;

if(flagM && GameObject.Find("lampleft"+status_f).transform.position.y<-100){
Destroy(GameObject.Find("lampleft"+status_f));
print("lampleft"+status_f+"destroyed");
status_f++;
}
if(status_f==j)
flagM=false;


if(flagg && GameObject.Find("lampright"+status_s).transform.position.y<-100){
Destroy(GameObject.Find("lampright"+status_s));
print("lampright"+status_s+"destroyed");
status_f++;
}
if(status_s==h)
flagM=false;


if(flagL)
{
dist=Vector3.Distance(GameObject.Find("ledge"+status_g).transform.position,player.transform.position);
if(dist>20){
	Destroy(GameObject.Find("ledge"+status_g));
	print("ledge"+status_g+"destroyed");
	status_g++;
}
}
if(status_g==k)
	flagL=false;
	
if(flagdie)
{

	highScore=valueGet();
	if(score>highScore)
		valueSet(score);
	flaggui=true;
}
}

 function OnGUI(){
	//GUI.Box(Rect(Screen.width-250,20,200,50),"No. of Obstacles Jumped :"+count);
	GUI.Box(Rect(20,20,200,50),"Distance:"+optscore+" meters");
	

	if(flaggui)
 {
    Screen.showCursor=true;
    Screen.lockCursor=false;
	
	GUI.Box(Rect(Screen.width - Screen.width/2-125,Screen.height - Screen.height/2-25,250,25), "Your Score is "+parseInt(score));
	GUI.Box(Rect(Screen.width - Screen.width/2-125,Screen.height - Screen.height/2+25,250,25),"The High Score"+valueGet());
	if(GUI.Button(Rect(Screen.width - Screen.width/2-125,Screen.height - Screen.height/2+50,250,25),"Back To Main Menu"))
		Application.LoadLevel("gui_main");
	}
	
}



function OnCollisionEnter(collision : Collision){
   if(collision.gameObject.tag=="Player")
   {
   		print("hit lamp");
   		audio.PlayOneShot(audio1);
   		flagdie=true;
   	
   		//Application.LoadLevel("intro_movie");
   }
   if(collision.gameObject.tag=="Respawn")
   {
   	print("hit ledge");
   	audio.PlayOneShot(audio2);
   	flagdie=true;
   }
  
  if(collision.gameObject.tag=="Finish")
  {
  	print("hit rock");
  	//audio.clip="";
  	audio.PlayOneShot(audio3);
  	flagdie=true;
  }
  
}



